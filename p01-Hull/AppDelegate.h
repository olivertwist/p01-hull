//
//  AppDelegate.h
//  p01-Hull
//
//  Created by Oliver Hull on 1/24/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

