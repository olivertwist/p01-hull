//
//  ViewController.m
//  p01-Hull
//
//  Created by Oliver Hull on 1/24/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloWorld;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clicked:(id)sender {
    
    [helloWorld setText:@"clicked"];
}

@end
